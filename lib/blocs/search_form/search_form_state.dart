part of 'search_form_cubit.dart';

class SearchFormState extends Equatable {
  final String word;
  final int delay;

  SearchFormState({required this.word, required this.delay});
  SearchFormState.initial() :
    word = '',
    delay = 1;

  @override
  List<Object?> get props => [word, delay];

  SearchFormState copyWith({
    String? word,
    int? delay,
  }) {
    return SearchFormState(
      word: word ?? this.word,
      delay: delay ?? this.delay,
    );
  }
}

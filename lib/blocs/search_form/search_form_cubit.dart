import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:search/blocs/tasks/tasks_cubit.dart';

part 'search_form_state.dart';

class SearchFormCubit extends Cubit<SearchFormState> {

  TasksCubit tasksCubit;

  SearchFormCubit({
    required this.tasksCubit
  }) : super(SearchFormState.initial());

  @override
  Future<void> close() {
    return super.close();
  }

  void setWord({required String word}) {
    emit(state.copyWith(
      word: word,
    ));
  }

  void setDelay({required int delay}) {
    emit(state.copyWith(
      delay: delay,
    ));
  }

  void sendForm() {
    tasksCubit.newTask(word: state.word, delay: state.delay);
  }
}

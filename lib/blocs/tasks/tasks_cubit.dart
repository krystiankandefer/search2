import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';
import 'package:search/data/models/models.dart';
import 'package:search/data/repositories/search_repository.dart';

part 'tasks_state.dart';
part 'tasks_cubit.freezed.dart';

class TasksCubit extends Cubit<TasksState> {

  TabController tabController;
  TasksCubit({required this.tabController}) : super(TasksState());

  final log = Logger('TasksCubit');
  final SearchRepository _searchRepository = GetIt.I<SearchRepository>();
  // skoro jednocześnie może działać i tak tylko jeden request to lepiej zrobić globaly timer,
  // gdyby mogło działac kilka na raz to zrobiłbym z Timerem w taskach
  // tylko też widze tu pewne ryzyko, rozjasu tasków i timera, bo gdy timer sie skończy będę aktualizował ten task inprogress
  Timer? _statusTimer;
  Timer get timer => _statusTimer ?? Timer.periodic(Duration.zero, (timer) { });

  @override
  Future<void> close() {
    _statusTimer?.cancel();
    return super.close();
  }

  @override
  void onChange(Change<TasksState> change) {
    super.onChange(change);
    log.finer('onChange -- ${this.runtimeType}, $change');
  }

  void newTask({required String word, required int delay}) async {
    if(delay < 0 || delay > 100){
      throw Exception('Wrong delay value');
    }

    cancel();

    Task newTask = Task(
      delay: delay,
      word: word,
      state: TaskState.inProgress,
    );
    List<Task> newStateTasks = List.from(state.tasks);
    emit(state.copyWith(
      tasks: newStateTasks..insert(0, newTask)
    ));
    _statusTimer = Timer.periodic(Duration(seconds: newTask.delay), (Timer t) async {
      if(t.isActive) {
        t.cancel();
      }
      try{
        ResultData resultData = await _searchRepository.getResult(newTask.word);
        //tu jednak jest ryzyko rozjazdu, trzeba zmienić na indywidualne timery, ale chyba ie zdążę w 4h
        emit(state.copyWith(
          tasks: List<Task>.from(state.tasks).map<Task>((Task t) => t.state != TaskState.inProgress? t : t.copyWith(
            state: TaskState.done,
            data: resultData,
          )).toList()
        ));
      }
      catch(e, s){
        emit(state.copyWith(
          tasks: List<Task>.from(state.tasks).map<Task>((Task t) => t.state != TaskState.inProgress? t : t.copyWith(
            state: TaskState.canceled,
            error: e,
          )).toList()
        ));
      }
    });
    tabController.animateTo(1);
  }

  void cancel() async {
    _statusTimer?.cancel();
    emit(state.copyWith(
      tasks: List<Task>.from(state.tasks).map<Task>((Task t) => t.copyWith(
        state: t.state == TaskState.inProgress? TaskState.canceled : t.state
      )).toList()
    ));
  }
}

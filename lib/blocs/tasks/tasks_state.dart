part of 'tasks_cubit.dart';

@freezed
class TasksState with _$TasksState {

  const factory TasksState([@Default(<Task>[]) List<Task> tasks]) = _TasksState;
}
import 'package:search/data/models/models.dart';
import 'package:search/data/retrofit/api.dart';
import 'package:search/data/retrofit/api_retrofit.dart';

abstract class SearchRepository {
  Future<ResultData> getResult(String word);
}

class SearchRepositoryImpl implements SearchRepository {

  final ApiRetrofit _apiRetrofit;

  Api get _api => _apiRetrofit.api;

  SearchRepositoryImpl(this._apiRetrofit);

  Future<ResultData> getResult(String word) async {
    SearchResponse response = await _api.getResult(word);
    return response.data;
  }
}
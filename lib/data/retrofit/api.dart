import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:search/data/models/models.dart';

part 'api.g.dart';

@RestApi()
abstract class Api {
  factory Api(Dio dio, {String baseUrl}) = _Api;

  @GET('/{word}.json')
  Future<SearchResponse> getResult(@Path("word") String word);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'result_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResultItem _$ResultItemFromJson(Map<String, dynamic> json) => ResultItem(
      json['url'] as String,
      json['title'] as String,
    );

Map<String, dynamic> _$ResultItemToJson(ResultItem instance) =>
    <String, dynamic>{
      'url': instance.url,
      'title': instance.title,
    };

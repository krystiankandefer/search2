import 'package:equatable/equatable.dart';
import 'package:search/data/models/models.dart';

enum TaskState {done, inProgress, canceled}

class Task extends Equatable{
  final String word;
  final int delay;
  final TaskState state;
  final ResultData? data;
  final Object? error;//może ewentualnie sam message? ale na razie zostawiam tak

  const Task({required this.word, required this.delay, required this.state, this.data, this.error})
    :assert(state != TaskState.done || data != null, 'Task done must have the data.');

  List<Object?> get props => [word, delay, state, data, error];

  Task copyWith({
    String? word,
    int? delay,
    TaskState? state,
    ResultData? data,
    Object? error,
  }) {
    return Task(
      word: word ?? this.word,
      delay: delay ?? this.delay,
      state: state ?? this.state,
      data: data ?? this.data,
      error: error ?? this.error,
    );
  }

  Task copyAndClearData() {
    return Task(
      word: this.word,
      delay: this.delay,
      state: this.state,
      data: null,
      error: this.error,
    );
  }

  Task copyAndClearError() {
    return Task(
      word: this.word,
      delay: this.delay,
      state: this.state,
      data: this.data,
      error: null,
    );
  }
}
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:search/data/models/models.dart';

part 'result_data.g.dart';

@JsonSerializable()
class ResultData extends Equatable{
  final String baseUrl;
  final List<ResultItem> items;

  const ResultData(this.baseUrl, {this.items = const []});

  List<Object?> get props => [baseUrl, items];

  factory ResultData.fromJson(Map<String, dynamic> json) => _$ResultDataFromJson(json);

  Map<String, dynamic> toJson() => _$ResultDataToJson(this);

}
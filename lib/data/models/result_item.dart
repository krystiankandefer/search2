import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'result_item.g.dart';

@JsonSerializable()
class ResultItem extends Equatable{
  final String url;
  final String title;

  const ResultItem(this.url, this.title);

  List<Object?> get props => [url, title];

  factory ResultItem.fromJson(Map<String, dynamic> json) => _$ResultItemFromJson(json);

  Map<String, dynamic> toJson() => _$ResultItemToJson(this);

}
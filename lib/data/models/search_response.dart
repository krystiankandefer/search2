import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:search/data/models/models.dart';

part 'search_response.g.dart';

@JsonSerializable()
class SearchResponse{
  final ResultData data;

  const SearchResponse(this.data);

  factory SearchResponse.fromJson(Map<String, dynamic> json) => _$SearchResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SearchResponseToJson(this);

}
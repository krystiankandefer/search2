import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search/blocs/tasks/tasks_cubit.dart';
import 'package:search/screens/main/result_tab.dart';
import 'package:search/screens/main/search_tab.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() {
    return _MainScreenState();
  }
}

class _MainScreenState extends State<MainScreen> with SingleTickerProviderStateMixin {

  TabController? _tabController;//celowo nullable zamiast late, ponieważ w takich sytuacjach wydaje sie bezpieczniejsze

  @override
  void initState() {
    _tabController = TabController(
      vsync: this,
      length: 2,
    );
    super.initState();
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TasksCubit>(
      create: (context) => TasksCubit(tabController: _tabController!),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Krystian Kandefer'),
          bottom: TabBar(
            labelPadding: EdgeInsets.symmetric(horizontal: 0),
            controller: _tabController,
            isScrollable: false,
            tabs: [
              Tab(text: 'Search'),
              Tab(text: 'Result'),
            ],
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            SearchTab(),
            ResultTab(),
          ],
        ),
      ),
    );
  }
}



import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search/blocs/search_form/search_form_cubit.dart';
import 'package:search/blocs/tasks/tasks_cubit.dart';
import 'package:search/data/models/models.dart';

class SearchTab extends StatefulWidget {
  SearchTab({Key? key}) : super(key: key);

  @override
  _SearchTabState createState() {
    return _SearchTabState();
  }
}

class _SearchTabState extends State<SearchTab> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final TextEditingController _wordController;

  @override
  void initState() {
    _wordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _wordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SearchFormCubit searchFormCubit = SearchFormCubit(
      tasksCubit: BlocProvider.of<TasksCubit>(context)
    );
    return BlocProvider<SearchFormCubit>(
      create: (context) => searchFormCubit,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      key: Key('word'),
                      controller: _wordController,
                      autofocus: true,
                      validator: (value){
                        if (value == null || value.isEmpty) {
                          return 'Word name is required';
                        }
                        else{
                          if (value != 'more' && value != 'normal') {
                            return 'Word can be "more" or "normal"';
                          }
                        }
                      },
                      onSaved: (String? value) {
                        if(value != null){
                          searchFormCubit.setWord(word: value);
                        }
                      },
                      decoration: InputDecoration(
                        hintText: 'Search for:',
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: SizedBox(
                      width: 80,
                      child: DropdownButtonFormField<int>(
                        value: searchFormCubit.state.delay,
                        icon: Icon(Icons.timelapse),
                        items: List<int>.generate(100, (i) => i + 1).map<DropdownMenuItem<int>>((int value) => DropdownMenuItem(
                          value: value,
                          child: Text(value.toString()),
                        )).toList(),
                        onChanged: (int? value) {
                          if(value != null){
                            searchFormCubit.setDelay(delay: value);
                          }
                        },
                        onSaved: (int? value) {
                          if(value != null){
                            searchFormCubit.setDelay(delay: value);
                          }
                        },
                      ),
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      if (_formKey.currentState != null && _formKey.currentState!.validate()) {
                        _formKey.currentState!.save();
                        searchFormCubit.sendForm();
                      }
                      // setState(() {});
                    },
                    icon: Icon(Icons.search),
                  ),
                ],
              ),
            ),
            Expanded(
              child: BlocBuilder<TasksCubit, TasksState>(
                builder: (context, tasksState){
                  return ListView.separated(
                    itemCount: tasksState.tasks.length,
                    separatorBuilder: (_, __) => Divider(),
                    itemBuilder: (_, index){
                      Task task = tasksState.tasks[index];
                      return ListTile(
                        onTap: () => BlocProvider.of<TasksCubit>(context).newTask(word: task.word, delay: task.delay),
                        leading: ((){
                          switch(task.state){
                            case TaskState.inProgress :
                              return Icon(Icons.timer);//TODO poszykać ikony z projektu
                            case TaskState.done :
                              return Icon(Icons.done);
                            case TaskState.canceled :
                              return Icon(Icons.cancel);
                          }
                        }()),
                        title: Text(task.word),
                        subtitle: Text('Duration: ${task.delay} seconds'),
                      );
                    }
                  );
                },
              )
            ),
          ],
        ),
      ),
    );
  }
}
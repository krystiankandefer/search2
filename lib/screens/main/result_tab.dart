import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search/blocs/tasks/tasks_cubit.dart';
import 'package:search/data/models/models.dart';
import 'package:transparent_image/transparent_image.dart';

class ResultTab extends StatefulWidget {
  ResultTab({Key? key}) : super(key: key);

  @override
  _ResultTabState createState() {
    return _ResultTabState();
  }
}

class _ResultTabState extends State<ResultTab> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TasksCubit, TasksState>(
      builder: (context, tasksState){
        Task? currentTask = tasksState.tasks.length == 0? null : tasksState.tasks.first;
        if(currentTask == null || currentTask.state == TaskState.done && currentTask.data == null){
          return const Center(child: Text('no url provided'),);
        }
        else{
          switch(currentTask.state){
            case TaskState.inProgress :
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircularProgressIndicator(value: 0.7),//TODO odczytać value z bloca ale niestety trzeba zrobić inaczej timer, tak aby dało się go słuchać
                    IconButton(
                      onPressed: () => BlocProvider.of<TasksCubit>(context).cancel(),
                      icon: Icon(Icons.cancel),
                    )
                  ],
                ),
              );
            case TaskState.canceled :
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('Request canceled'),
                    TextButton(
                      onPressed: () => BlocProvider.of<TasksCubit>(context).newTask(word: currentTask.word, delay: currentTask.delay),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.restart_alt_outlined),
                          Text('Restart')
                        ],
                      )
                    )
                  ],
                ),
              );
            case TaskState.done :
              return ListView.separated(
                itemCount: currentTask.data!.items.length,
                separatorBuilder: (_, __) => Divider(),
                itemBuilder: (_, index){
                  ResultItem item = currentTask.data!.items[index];
                  return ListTile(
                    onTap: () => {},
                    leading: FadeInImage.memoryNetwork(
                      image: 'http://sirocco.home.pl/guestftp/' + item.url,
                      placeholder: kTransparentImage,
                      imageErrorBuilder: (context, error, stackTrace) => Image.asset(
                        'assets/images/logo.png',
                        fit: BoxFit.contain,
                        height: 50,
                        width: 50,
                      ),
                      fit: BoxFit.cover,
                      height: 50,
                      width: 50,
                    ),
                    title: Text(item.title),
                  );
                }
              );
          }
        }
      },
    );
  }
}
import 'package:flutter/material.dart';
import 'package:search/data/repositories/search_repository.dart';
import 'package:search/data/retrofit/api_retrofit.dart';
import 'package:search/screens/main/main_screen.dart';
import 'package:logging/logging.dart';
import 'package:get_it/get_it.dart';

void main() {

  WidgetsFlutterBinding.ensureInitialized();

  GetIt.I.registerLazySingleton<ApiRetrofit>(() => ApiRetrofitImpl());
  GetIt.I.registerSingleton<SearchRepository>(SearchRepositoryImpl(GetIt.I<ApiRetrofit>()));

  Logger.root.level = Level.ALL;
  Logger.root.onRecord.where((record) => record.level >= Level.WARNING).listen((record) {
    print('${record.level.name}: ${record.time}: ${record.message}');
    print('${record.error}, ${record.stackTrace}');
  });

  // Logowanie requestów
  Logger.root.onRecord.where((record) => record.loggerName == 'ApiRetrofit').listen((record) {
    print('${record.message}');
  });

  Logger.root.onRecord.where((record) => record.loggerName == 'TasksCubit').listen((record) {
    print('${record.message}');
  });

  runApp(FutureBuilder(
    future: GetIt.instance.allReady(),
    builder: (BuildContext context, AsyncSnapshot snapshot) {
      if (snapshot.hasData) {
        return MyApp();
      } else {
        return Material(
          child: Center(
            child: Image.asset(
              'assets/images/logo.png',
              height: 53,
            ),
          )
        );
      }
    }
  ));
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Search',
      theme: ThemeData(
        primarySwatch: Colors.green,//w zadaniu nie było mowy o pełnym theme dlatego pomijam
      ),
      // zadanie nie wymaga navigatora, całość będize na jednej trasie
      home: MainScreen(),
    );
  }
}

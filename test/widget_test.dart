// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:search/data/repositories/search_repository.dart';
import 'package:search/data/retrofit/api_retrofit.dart';
import 'package:search/main.dart';

void main() {

  setUpAll(() {
    setupDependencyInjection();
  });

  testWidgets('Initialize clear app smoke test', (WidgetTester tester) async {

    //taki tylko prosty test czy aplikacja wystartowała
    await tester.pumpWidget(MyApp());
    expect(find.text('Krystian Kandefer'), findsOneWidget);
  });
}

void setupDependencyInjection() {
  GetIt.I.registerLazySingleton<ApiRetrofit>(() => ApiRetrofitImpl());
  GetIt.I.registerSingleton<SearchRepository>(SearchRepositoryImpl(GetIt.I<ApiRetrofit>()));
}

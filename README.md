# search

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Przydatne polecenia

### generator
`flutter pub run build_runner watch --delete-conflicting-outputs`

### budowanie
`flutter build appbundle --target lib/main.dart --release` - App Bundle
`flutter build apk --target lib/main.dart --release` - APK
